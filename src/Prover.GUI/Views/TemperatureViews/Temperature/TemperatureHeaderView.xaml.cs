﻿using System.Windows.Controls;

namespace Prover.GUI.Views.TemperatureViews.Temperature
{
    /// <summary>
    /// Interaction logic for TemperatureHeader.xaml
    /// </summary>
    public partial class TemperatureHeaderView : UserControl
    {
        public TemperatureHeaderView()
        {
            InitializeComponent();
        }
    }
}
