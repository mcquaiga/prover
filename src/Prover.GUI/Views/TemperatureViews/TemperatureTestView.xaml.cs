﻿using System.Windows.Controls;

namespace Prover.GUI.Views.TemperatureViews
{
    /// <summary>
    /// Interaction logic for TemperatureTestView.xaml
    /// </summary>
    public partial class TemperatureTestView : UserControl
    {
        public TemperatureTestView()
        {
            InitializeComponent();
        }
    }
}
