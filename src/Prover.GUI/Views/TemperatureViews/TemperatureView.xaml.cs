﻿using System.Windows.Controls;

namespace Prover.GUI.Views.TemperatureViews
{
    /// <summary>
    /// Interaction logic for TemperatureView.xaml
    /// </summary>
    public partial class TemperatureView : UserControl
    {
        public TemperatureView()
        {
            InitializeComponent();
        }
    }
}
