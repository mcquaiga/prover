﻿using System.Windows;
using System.Windows.Controls;

namespace Prover.GUI.Views.TemperatureViews
{
    /// <summary>
    /// Interaction logic for LiveTemperatureReadView.xaml
    /// </summary>
    public partial class LiveTemperatureReadView : UserControl
    {
        public LiveTemperatureReadView()
        {
            InitializeComponent();
        }
    }
}
