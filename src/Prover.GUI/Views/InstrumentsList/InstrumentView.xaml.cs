﻿using System.Windows.Controls;

namespace Prover.GUI.Views.InstrumentsList
{
    /// <summary>
    /// Interaction logic for InstrumentView.xaml
    /// </summary>
    public partial class InstrumentView : UserControl
    {
        public InstrumentView()
        {
            InitializeComponent();
        }
    }
}
