﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Prover.GUI.Views.InstrumentsList
{
    /// <summary>
    /// Interaction logic for TempTestView.xaml
    /// </summary>
    public partial class InstrumentTempTestView : UserControl
    {
        public InstrumentTempTestView()
        {
            InitializeComponent();
        }
    }
}
