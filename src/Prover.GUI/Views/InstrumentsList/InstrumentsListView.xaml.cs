﻿using System.Windows.Controls;

namespace Prover.GUI.Views.InstrumentsList
{
    /// <summary>
    /// Interaction logic for InstrumentsListView.xaml
    /// </summary>
    public partial class InstrumentsListView : UserControl
    {
        public InstrumentsListView()
        {
            InitializeComponent();
        }
    }
}
